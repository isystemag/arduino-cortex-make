# Arduino/Genuino Makefile for Cortex based boards #

This project is focused on the development of a Makefile to build Arduino/Genuino projects for the following boards:

* [Arduino M0 Pro](http://www.arduino.org/products/boards/4-arduino-boards/arduino-m0-pro)
* [Arduino/Genuino Zero](https://www.arduino.cc/en/Main/ArduinoBoardZero)
* [Arduino Due](http://www.arduino.org/products/boards/4-arduino-boards/arduino-due)

The aim is to make it possible to build Arduino-based projects within a typical IDE such as Eclipse. With the Arduino M0 Pro/Zero and Genuino Zero, it is then possible to debug the MCU via the on-board EDBG debugger using SWD (from Atmel) or a seperate debug tool such as the iTAG.FIFTY of iTAG.2K from iSYSTEM.

### What is this repository for? ###

* Creation of a Makefile for building Arduino/Genuino-based projects that feature ARM Cortex processors.
* Simplify integration of Arduino/Genuino projects into IDEs, such as Eclipse and winIDEA, so that developers can also debug Arduino applications.

### How does it work? ###

Quite simply, write your Arduino sketch and save it in a <project directory>/src. Then, from a Windows command line or Linux shell, type:

make <a few parameters here>

and your Arduino sketch will be compiled! The resulting ELF file in the "Debug" or "Release" folder can then be included in your favourite IDE.

Want to know how to use the Makefile? Simply type "make help" for a list of possible parameters.

### What is supported? ###

Currently the following Arduino IDEs and core Arduino functionality are supported on the following platforms:

* Windows:
    * Arduino IDE from arduino.org - Versions 1.7.0 through 1.7.11 supported and fully tested
    * Arduino IDE from arduino.cc - Zero supported from packages 1.6.2 through 1.6.7 and fully tested
    * Arduino IDE from arduino.cc - DUE supported from packages 1.6.2 through 1.6.8 and partially tested
* Linux:
    * As above but not tested
* OS X:
    * As above but not tested

Currently the following ARM Cortex-M boards are supported in conjunction with the Arduino IDE distributions supported above:

* Arduino M0 Pro - supported and tested (Windows only)
* Arduino DUE - supported and tested (Windows only)
* Arduino Zero - supported and tested (Windows only)
* Genuio Zero - supported and tested (Windows only)

Currently the following in-built libraries are supported:
* Servo - supported and tested on Arduino DUE
* All other Cortex-M relevant libraries in Arduino.org are supported but not tested
* Cortex-M relevant libraries in Arduino.cc are not supported as yet.

We also support building a sketch to allow program trace with the Micro Trace Buffer (MTB). This requires that you also copy modified linker scripts (files with extension .ld) to your project folder. The modified linker scripts are located in the 'release' folder.

### What isn't supported? ###

Arduino's featuring the 8-bit AVR core, such as ATmega and ATtiny, fitted to boards such as the Uno or Mega, are not supported. There is no plan to extend this makefile to support 8-bit Arduino boards.

### How do I get set up? ###

This is a brief summary of what you need to make this work for you:

1. Download and install the Arduino IDE: you need this so that you have the Arduino core source code available and, optionally, their GCC compiler and the various libraries they offer. The Arduino IDE can be downloaded here:
    1. from arduino.org [link](http://www.arduino.org/software#ide)
    2. or from arduino.cc [link](https://www.arduino.cc/en/Main/Software)
2. (Step for arduino.cc IDE only) You will need to install support for the Arduino/Genuino Zero and/or DUE baords through the "Boards Manager". In the IDE select "Tools->Board...->Board Manager" and install the "Arduino SAM Boards" (for DUE) and "Arduino SAMD Boards" (for ZERO) packages.
3. (Optional) Download [GNU GCC compiler](https://gcc.gnu.org/mirrors.html) if you wish to build with a different GCC compiler.
4. Ensure you have 'make' on your system. On Windows systems we have had success installing [MinGW](http://www.mingw.org/) with instructions for installtion [here](http://isy.si/mingw-install). make can then be found in (using default install) here-> C:\MinGW\msys\1.0\bin\make.exe
5. Download an IDE - we are currently focused on [winIDEA Open](http://isy.si/winideaopen). If you know Eclipse, it should work for you too.
6. Download the 'Makefile' from this repository. Version recommendation - please trial either the newest development version in the 'dev_xx_xx' folder or the Makefile in the 'release' folder which should be more robust. Copy this file into your project directory.
7. Create a sketch in the Arduino IDE. Build and program it to your target board to make sure it works.
8. Create a winIDEA Open workspace in the same folder. Create a new subdirectory named 'src'. Copy your *.ino file into the '.\src' folder and rename the file copy with the extension '.ccp'. Additionally, add the code '#include "Arduino.h"' to the top of your source code file.
9. Also add a "Build" and "Release" folder to your project directory.
10. Use make to build the application. Type 'make help' for instructions on using this Makefile. Use the "Makefile" that can be found in the "release" directory as this is, to our best knowledge, stable. The files in the "dev_xx_xx" directories are development versions.

This is currently a work in progress (Nov 2016). Just drop me an email (see below) if you need more help until we have more details here.

### Contribution guidelines ###

* Either use bitbucket to submit a pull request or drop me an email (see below).
* Submit an issue via bitbucket

### Who do I talk to? ###

* This repository is administered by Stuart Cording from iSYSTEM AG. Please contact via inews@isystem.com
* Please share experiences, requests for features, improvements - thanks!

### Learn markdown ###
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)