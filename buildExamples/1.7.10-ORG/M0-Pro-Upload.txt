C:\Arduino\arduino-1.7.10-ORG/hardware/tools/OpenOCD-0.9.0-arduino/bin/openocd.exe -s C:\Arduino\arduino-1.7.10-ORG/hardware/tools/OpenOCD-0.9.0-arduino/share/openocd/scripts/ -f ../../../../../arduino/samd/variants/arduino_zero/openocd_scripts/arduino_zero.cfg -c program {{C:\Users\Stuart\AppData\Local\Temp\build9121330695645578745.tmp/ASCIITable.cpp.bin}} verify 0x4000 reset exit 
Open On-Chip Debugger 0.9.0 (2015-05-26-16:17)
Licensed under GNU GPL v2
For bug reports, read
	http://openocd.org/doc/doxygen/bugs.html
Info : only one transport option; autoselect 'swd'
adapter speed: 500 kHz
adapter_nsrst_delay: 100
cortex_m reset_config sysresetreq
Info : CMSIS-DAP: SWD  Supported
Info : CMSIS-DAP: JTAG Supported
Info : CMSIS-DAP: Interface Initialised (SWD)
Info : CMSIS-DAP: FW Version = 01.1F.0118
Info : SWCLK/TCK = 1 SWDIO/TMS = 1 TDI = 1 TDO = 1 nTRST = 0 nRESET = 1
Info : CMSIS-DAP: Interface ready
Info : clock speed 500 kHz
Info : SWD IDCODE 0x0bc11477
Info : at91samd21g18.cpu: hardware has 4 breakpoints, 2 watchpoints
target state: halted
target halted due to debug-request, current mode: Thread 
xPSR: 0x61000000 pc: 0x000028f4 msp: 0x20002c00
** Programming Started **
auto erase enabled
Info : SAMD MCU: SAMD21G18A (256KB Flash, 32KB RAM)
wrote 16384 bytes from file C:\Users\Stuart\AppData\Local\Temp\build9121330695645578745.tmp/ASCIITable.cpp.bin in 2.247345s (7.120 KiB/s)
** Programming Finished **
** Verify Started **
verified 8212 bytes in 0.697383s (11.499 KiB/s)
** Verified OK **
** Resetting Target **
shutdown command invoked
